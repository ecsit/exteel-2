﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KiteLion.Debugging;

public class Main : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("CBUG_ON", 1);
        CBUG.Do("TEST 101");
    }

    // Update is called once per frame
    void Update()
    {
        CBUG.Do("BLAH");
    }
}
