﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KiteLion.Common;
using KiteLion.Debugging;

public class Launch : MonoBehaviour
{
    PhotonArenaManager PM;
    public GameObject spawn;
    private bool isSpawned;

    private GameObject player;

    //todo Proper Scene Construction Scripting

    public float TESTGravityForce;
    private float prevTESTGForce;

    // Start is called before the first frame update
    void Start()
    {
        PM = PhotonArenaManager.Instance;
        PM.ConnectAndJoinRoom("TESTME", null);
        isSpawned = false;
        //Forces.G = GravityForce; //todo improve "forces" layout
    }

    // Update is called once per frame
    void Update()
    {
        if (PM.CurrentServerUserDepth == PhotonArenaManager.ServerDepthLevel.InRoom && isSpawned == false) {
            player = PM.SpawnPlayer(spawn.transform.position, spawn.transform.rotation, "RahuIII");
            //Tools.DelayFunction(new Tools.VanillaFunction(SetExternalForce), 0);
            
            isSpawned = true;
        }

        if(TESTGravityForce != prevTESTGForce) {
            prevTESTGForce = TESTGravityForce;
            Forces.G = TESTGravityForce;
            //todo GRAVITY IS CURRENTLY LINEAR (BAD BAD BAD)!~!!
        }

    }

    //public void SetExternalForce() {
    //    player.GetComponent<ControllerMecha1>().ExternalForces += () => {
    //        CBUG.Do("TEST44444" + Forces.Gravity3);
    //        return Forces.Gravity3;
    //    };
    //    CBUG.Do("HEYYYYYYYYYYYYYYYYYYYYYYYYYYYYY" + Time.time);
    //}
}
