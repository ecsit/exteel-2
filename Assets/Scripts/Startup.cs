﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using KiteLion.Debugging;
using KiteLion.Common;

/// <summary>
/// Manages game load behavior. Everything before player can interact.
/// </summary>
public class Startup : MonoBehaviour
{

    public GameObject DownloadUpdateOverlay;

    void Start() {
        // A correct website page.
        StartCoroutine(GetRequest(@"http://kitelion.me/MechSteel2/Version.txt"));
    }

    IEnumerator GetRequest(string uri) {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri)) {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string test = "dsd  dssd ";
            test = Tools.RemoveWhitespace(test);
            if (webRequest.isNetworkError) {
                CBUG.Do("Error getting Version: " + webRequest.error);
            }
            else {
                string result = System.Text.Encoding.UTF8.GetString(webRequest.downloadHandler.data);
                result = Tools.RemoveWhitespace(result);

                int isEqual = 0;
                if(result.CompareTo(Application.version) == isEqual) {
                    CBUG.Do($"Latest Game Version: {result}");
                } else {
                    CBUG.Do($"Incorrect version. Latest: {result} Yours: {Application.version} ");
                    DownloadUpdateOverlay.SetActive(true); //todo re-enable and get overlay checking text working right!
                    Cursor.lockState = CursorLockMode.None;
                }
            }
        }
    }
}
