﻿using KiteLion.Debugging;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class INFO : MonoBehaviour
{

    bool infoGiven;

    // Start is called before the first frame update
    void Start()
    {
        infoGiven = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!infoGiven) {
            CBUG.Do("WASD TO MOVE -- SPACEBAR TO JUMP");
            infoGiven = true;
        }
    }
}
